<?php

class Lepape_AceEditor_Model_Source_EditableXmlTextAreas
{
    public function toOptionArray()
    {
        $helper = Mage::helper('ace_editor');
        return array(
            array('value' => '#page_layout_update_xml', 'label' => $helper->__('CMS Page Layout Update')),
            array('value' => '#page_custom_layout_update_xml', 'label' => $helper->__('CMS Page Custom Layout Update')),
            array('value' => '#group_10custom_layout_update', 'label' => $helper->__('Category Custom Layout Update'))
        );
    }
}

