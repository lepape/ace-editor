<?php

class Lepape_AceEditor_Model_Source_TabSize
{
    public function toOptionArray()
    {
        return array(
            array('value' => '1', 'label' => '1'),
            array('value' => '2', 'label' => '2'),
            array('value' => '4', 'label' => '4')
        );
    }
}